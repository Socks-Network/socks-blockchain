# Socks Blockchain

![Socks Network Logo](http://socks.works/img/socks-network-192-round.png)

This is a fork of chia-network 1.2.0

You can start farming some SOCKS using the existing Chia™ plots.


---------------------------

ATTENTION: Most of the links are broken here due to the urgent move from the GitHub


------------


## Download it now

Please grab the recent build for your OS from the [releases page](https://bitbucket.org/Socks-Network/socks-blockchain/downloads/)

[![Download Socks Blockchain Link](http://socks.works/img/download-button.png)](https://bitbucket.org/Socks-Network/socks-blockchain/downloads/)


### Why do we need an another fork?

* Forks help to **reuse existing** resources (plots) reducing waste.
* **5Gb Plots** support. You've got 50Gb left on each of your HDD and can't fill them up? This fork allows at k=28(~5Gb) plots. Again, reducing waste. You can farm using your space from the mobile phone!
* Whales brought Chia Blockchain netspace to the moon very quickly. Average farmer with spare 10-20Tb simply could not get rewarded for their netspace contribution
* We do our best to make this SOCKS fork **transparent and auditable**, take a look at the initial [Pull Request](https://github.com/Socks-Network/socks-blockchain/pull/3).
* Most the of the forks **got no updates** since launch. We're not going to abandon Socks Network.

## Fair rewards schedule

Unlike other forks SOCKS is friendly to the late joiners:

- The first day of launch block reward is 0.5 SOCK
- The week after 1 SOCK
- Then, the pair of SOCK for next 3 years


--------------

Get Involved: 
[Socket-Blockchain repository](https://bitbucket.org/Socks-Network/socks-blockchain/downloads/) | 
[/r/SocksNetwork](https://www.reddit.com/r/SocksNetwork/) |
[http://socks.works](http://socks.works) |
[@Socks_Network](https://twitter.com/Socks_Network) 


## Credits

This fork is based on the [Chia-Network](https://github.com/Chia-Network), their team is the one who made it possible.


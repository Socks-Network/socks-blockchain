from setuptools import setup

dependencies = [
    "blspy==1.0.5",  # Signature library
    "chiavdf==1.0.2",  # timelord and vdf verification
    "chiabip158==1.0",  # bip158-style wallet filters
    "chiapos==1.0.4",  # proof of space
    "clvm==0.9.7",
    "clvm_rs==0.1.8",
    "clvm_tools==0.4.3",
    "aiohttp==3.7.4",  # HTTP server for full node rpc
    "aiosqlite==0.17.0",  # asyncio wrapper for sqlite, to store blocks
    "bitstring==3.1.7",  # Binary data management library
    "colorlog==5.0.1",  # Adds color to logs
    "concurrent-log-handler==0.9.19",  # Concurrently log and rotate logs
    "cryptography==3.4.7",  # Python cryptography library for TLS - keyring conflict
    "keyring==23.0.1",  # Store keys in MacOS Keychain, Windows Credential Locker
    "keyrings.cryptfile==1.3.4",  # Secure storage for keys on Linux (Will be replaced)
    #  "keyrings.cryptfile==1.3.8",  # Secure storage for keys on Linux (Will be replaced)
    #  See https://github.com/frispete/keyrings.cryptfile/issues/15
    "PyYAML==5.4.1",  # Used for config file format
    "setproctitle==1.2.2",  # Gives the socks processes readable names
    "sortedcontainers==2.3.0",  # For maintaining sorted mempools
    "websockets==8.1.0",  # For use in wallet RPC and electron UI
    "click==7.1.2",  # For the CLI
    "dnspython==2.1.0",  # Query DNS seeds
]

upnp_dependencies = [
    "miniupnpc==2.0.2",  # Allows users to open ports on their router
]

dev_dependencies = [
    "pytest",
    "pytest-asyncio",
    "flake8",
    "mypy",
    "black",
    "aiohttp_cors",  # For blackd
    "ipython",  # For asyncio debugging
]

kwargs = dict(
    name="socks-blockchain",
    author="Mariano Sorgente",
    author_email="mariano@socks.works",
    description="Socks blockchain full node, farmer, timelord, and wallet.",
    url="https://socks.works/",
    license="Apache License",
    python_requires=">=3.7, <4",
    keywords="socks blockchain node",
    install_requires=dependencies,
    setup_requires=["setuptools_scm"],
    extras_require=dict(
        uvloop=["uvloop"],
        dev=dev_dependencies,
        upnp=upnp_dependencies,
    ),
    packages=[
        "build_scripts",
        "socks",
        "socks.cmds",
        "socks.clvm",
        "socks.consensus",
        "socks.daemon",
        "socks.full_node",
        "socks.timelord",
        "socks.farmer",
        "socks.harvester",
        "socks.introducer",
        "socks.plotting",
        "socks.pools",
        "socks.protocols",
        "socks.rpc",
        "socks.server",
        "socks.simulator",
        "socks.types.blockchain_format",
        "socks.types",
        "socks.util",
        "socks.wallet",
        "socks.wallet.puzzles",
        "socks.wallet.rl_wallet",
        "socks.wallet.cc_wallet",
        "socks.wallet.did_wallet",
        "socks.wallet.settings",
        "socks.wallet.trading",
        "socks.wallet.util",
        "socks.ssl",
        "mozilla-ca",
    ],
    entry_points={
        "console_scripts": [
            "socks = socks.cmds.socks:main",
            "socks_wallet = socks.server.start_wallet:main",
            "socks_full_node = socks.server.start_full_node:main",
            "socks_harvester = socks.server.start_harvester:main",
            "socks_farmer = socks.server.start_farmer:main",
            "socks_introducer = socks.server.start_introducer:main",
            "socks_timelord = socks.server.start_timelord:main",
            "socks_timelord_launcher = socks.timelord.timelord_launcher:main",
            "socks_full_node_simulator = socks.simulator.start_simulator:main",
        ]
    },
    package_data={
        "socks": ["pyinstaller.spec"],
        "": ["*.clvm", "*.clvm.hex", "*.clib", "*.clinc", "*.clsp"],
        "socks.util": ["initial-*.yaml", "english.txt"],
        "socks.ssl": ["socks_ca.crt", "socks_ca.key", "dst_root_ca.pem"],
        "mozilla-ca": ["cacert.pem"],
    },
    use_scm_version={"fallback_version": "unknown-no-.git-directory"},
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    zip_safe=False,
)


if __name__ == "__main__":
    setup(**kwargs)

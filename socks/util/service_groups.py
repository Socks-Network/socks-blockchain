from typing import KeysView, Generator

SERVICES_FOR_GROUP = {
    "all": "socks_harvester socks_timelord_launcher socks_timelord socks_farmer socks_full_node socks_wallet".split(),
    "node": "socks_full_node".split(),
    "harvester": "socks_harvester".split(),
    "farmer": "socks_harvester socks_farmer socks_full_node socks_wallet".split(),
    "farmer-no-wallet": "socks_harvester socks_farmer socks_full_node".split(),
    "farmer-only": "socks_farmer".split(),
    "timelord": "socks_timelord_launcher socks_timelord socks_full_node".split(),
    "timelord-only": "socks_timelord".split(),
    "timelord-launcher-only": "socks_timelord_launcher".split(),
    "wallet": "socks_wallet socks_full_node".split(),
    "wallet-only": "socks_wallet".split(),
    "introducer": "socks_introducer".split(),
    "simulator": "socks_full_node_simulator".split(),
}


def all_groups() -> KeysView[str]:
    return SERVICES_FOR_GROUP.keys()


def services_for_groups(groups) -> Generator[str, None, None]:
    for group in groups:
        for service in SERVICES_FOR_GROUP[group]:
            yield service


def validate_service(service: str) -> bool:
    return any(service in _ for _ in SERVICES_FOR_GROUP.values())

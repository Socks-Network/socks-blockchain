from typing import Dict

# The rest of the codebase uses msocks everywhere.
# Only use these units for user facing interfaces.
units: Dict[str, int] = {
    "socks": 10 ** 12,  # 1 socks (XCH) is 1,000,000,000,000 msock (1 trillion)
    "msock:": 1,
    "colouredcoin": 10 ** 3,  # 1 coloured coin is 1000 colouredcoin msocks
}
